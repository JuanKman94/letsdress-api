# Let's Dress - API

This project was generated using `$ rails new letsdress-api --api`, I followed
[this tutorial](https://www.simplify.ba/articles/2016/06/18/creating-rails5-api-only-application-following-jsonapi-specification/).

## Docker

The docker-compose configuration requires an image named `letsdress`,
which must be built with the present [Dockerfile](Dockerfile), perhaps we
upload the image to a public registry later.

See the [compose reference](https://docs.docker.com/compose/compose-file/) for
your docker version.

```bash
$ docker-compose build
```

## Testing

ThoughtBot provides an
[interesting read](https://robots.thoughtbot.com/how-we-test-rails-applications)
about testing, they are the writers of
[Paperclip](https://github.com/thoughtbot/factory_bot) and
[FactoryBot](https://github.com/thoughtbot/paperclip).

Using docker-compose:

```bash
$ docker-compose up test
```

The tests are written with [RSpec](http://rspec.info) (documentation can be
found at [relish](https://relishapp.com/rspec/rspec-expectations/v/3-7/docs))
using
[factories](https://github.com/thoughtbot/factory_bot/blob/master/GETTING_STARTED.md)
instead of fixtures.
