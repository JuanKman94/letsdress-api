Rails.application.routes.draw do
  root 'application#root'

  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users
  resources :dresses
end
