FROM ruby:2.4.0

EXPOSE 3000

RUN apt-get update -qq && \
	apt-get install -yqq build-essential libpq-dev ghostscript

# Install node's upstream
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
	apt-get install nodejs -y

RUN mkdir /usr/src/app
WORKDIR /usr/src/app

ADD Gemfile /usr/src/app/Gemfile
ADD Gemfile.lock /usr/src/app/Gemfile.lock

ADD . /usr/src/app

RUN gem install bundler rspec --no-ri --no-rdoc && \
	bundle install --path vendor

CMD [ "rails", "server" ]
