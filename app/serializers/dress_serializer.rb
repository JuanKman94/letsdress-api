class DressSerializer < ActiveModel::Serializer
  attributes :id, :text, :price, :created_at
  belongs_to :user
end
