class ApplicationController < ActionController::API

  before_action :check_header
  before_action :validate_type, only: [ :create, :update ]

  def root
    # TODO: take API version from config somewhere
    render json: {
          version: 0.1,
          name: 'LetsDress API'
      },
      status: :ok
  end

  private

  def render_error(resource, status)
    render json: resource,
      status: status,
      adapter: :json_api,
      serializer: ActiveModel::Serializer::ErrorSerializer
  end

  def check_header
    if %w( POST PUT PATCH ).include? request.method
      if request.content_type != 'application/vnd.api+json'
        head :not_acceptable and return
      end
    end
  end

  def validate_type
    if params['data'] && params['data']['type']
      if params['data']['type'] == params[:controller]
        return true
      end
    end

    head :conflict and return
  end

  def default_meta
    {
      version: 0.1, # TODO: take API version from config somewhere
      authors: [ 'JuanKman94' ]
    }
  end

  def pagination_meta(object)
    {
      current_page: object.current_page,
      next_page: object.next_page,
      prev_page: object.prev_page,
      total_pages: object.total_pages,
      total_count: object.total_count,
    }
  end

end
