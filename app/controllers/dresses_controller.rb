class DressesController < ApplicationController

  before_action :authenticate_user!
  before_action :set_dress, only: [ :show, :update, :destroy ]

  # GET /dresses
  # GET /dresses.json
  def index
    Rails.logger.debug "dresses#index: params => #{ params }"
    dresses = Dress.all

    # sort results
    if params[:sort]
      # TODO: treat each sort filter
      f = params[:sort].split(',').first
      field = f[0] == '-' ? f[1..-1] : f
      order = f[0] == '-' ? 'DESC' : 'ASC'


      if Dress.new.has_attribute?(field)
        # HINT: make a hash with each sort filter:
        #   { field.downcase.to_sym => order.downcase.to_sym }
        dresses = dresses.order("#{field} #{order}")
      end
    end

    dresses = dresses.page(params[:page] ? params[:page][:number] : 1)

    render json: dresses, meta: pagination_meta(dresses)
  end

  # GET /dresses/1
  # GET /dresses/1.json
  def show
    render json: @dress, include: [ 'user' ]
  end

  # POST /dresses
  # POST /dresses.json
  def create
    dress = Dress.new(dress_params)
    dress.user = current_user

    if dress.save
      render json: dress,
             status: :created,
             location: dress
    else
      render json: { errors: dress.errors.full_messages },
             status: :unprocessable_entity
    end
  end

  # PATCH/PUT /dresses/1
  # PATCH/PUT /dresses/1.json
  def update
    begin
      raise Exception if not @dress.update(dress_params)
      render json: @dress, status: :ok and return
    rescue => ex
      dress = Dress.new
      dress.errors.add(:exception, ex)
    end

    render_error(dress, :unprocessable_entity)
  end

  # DELETE /dresses/1
  # DELETE /dresses/1.json
  def destroy
    if @dress.destroy
      head :no_content
    else
      render_error(@dress, :internal_server_error)
    end
  end

  private

  def set_dress
    begin
      @dress = Dress.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      dress = Dress.new
      dress.errors.add(:id, 'Invalid dress ID')
      render_error(dress, :not_found) and return
    end
  end

  def dress_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params)
  end

end
