class UsersController < ApplicationController

  before_action :authenticate_user!
  before_action :set_user, only: [ :show, :update, :destroy ]

  def index
    users = User.page(params[:page] ? params[:page][:number] : 1)
    render json: users, meta: pagination_meta(users)
  end

  def show
    render json: @user
  end

  def create
    user = User.new(user_params)

    if user.save
      render json: user, status: :created
    else
      render_error(user, :unprocessable_entity)
    end
  end

  def update
    begin
      raise Exception if not @user.update(user_params)
      render json: @user, status: :ok and return
    rescue => ex
      user = User.new
      user.errors.add(:exception, ex)
    end

    render_error(user, :unprocessable_entity)
  end

  def destroy
    if @user.destroy
      head :no_content
    else
      render_error(@user, :internal_server_error)
    end
  end

  private

  def set_user
    begin
      @user = User.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      user = User.new
      user.errors.add(:id, 'Invalid user ID')
      render_error(user, :not_found) and return
    end
  end

  def user_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params)
  end

end
