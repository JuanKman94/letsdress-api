FactoryBot.define do
  sequence :email do |n|
    "email_#{ n }@email.com"
  end

  factory :user do
    email
    password 'super secret'
    password_confirmation 'super secret'

    factory :user_with_dresses do
      transient do
        dresses_count 5
      end

      after(:create) do |user, evaluator|
        create_list(:dress, evaluator.dresses_count, user: user)
      end
    end
  end

  factory :dress do
    user

    text "My random description of a dress"
    price { (100.0 - 10.0) * rand() + 10.0 }
  end
end
