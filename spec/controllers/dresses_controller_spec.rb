require 'rails_helper'

RSpec.describe DressesController, type: :controller do

  describe 'GET #index' do
    it 'redirects to sign_in' do
      get :index

      expect(response.redirection?).to be(true)
    end

    it 'returns a valid list of dresses with pagination' do
      user = create(:user)
      create_list(:dress, Dress.default_per_page * 2)

      sign_in user
      get :index

      expect(response).to have_http_status(:ok)
      resp_data = JSON.parse(response.body)
      Rails.logger.debug "resp_data = #{ resp_data }"

      expect(resp_data['data'].length).to eq(Dress.default_per_page)
      expect(resp_data['data'][0]['type']).to eq('dresses')

      # links
      expect(resp_data['links']['prev']).to be(nil)
      expect(resp_data['links']['self']).to eq(resp_data['links']['first'])

      # pagination meta
      expect(resp_data['meta']['prev-page']).to be(nil)
      expect(resp_data['meta']['current-page']).to eq(1)
      expect(resp_data['meta']['next-page']).to eq(2)
      expect(resp_data['meta']['total-pages']).to eq(2)
      expect(resp_data['meta']['total-count']).to eq(Dress.count)
    end

    it 'sorts properly by price' do
      user = create(:user)
      dresses = create_list(:dress, 10)

      sign_in user
      get :index, params: { sort: '-price' }

      expect(response).to have_http_status(:ok)

      resp_data = JSON.parse(response.body)
      highest_price_dress = dresses.sort { |a, b| a.price <=> b.price }.last

      expect(resp_data['data'][0]['id']).to eq(highest_price_dress.id.to_s)
    end

    pending 'sorts with more than one filter'
  end

  describe 'GET #show' do
    it 'returns dress by id' do
      dress = create(:dress)

      sign_in dress.user
      get :show, format: :json, params: { id: dress.id }

      expect(response).to have_http_status(:success)
      json_resp = JSON.parse(response.body)

      expect(json_resp['data']['id']).to eql(dress.id.to_s)
    end
  end

  describe 'POST #create' do
    it 'fails with 401 - UNAUTHORIZED without signing in' do
      request.headers['Content-Type'] = 'application/vnd.api+json'
      post :create, format: :json, params: {
        data: {
          type: 'dresses',
          attributes: attributes_for(:dress)
        }
      }

      expect(response).to have_http_status(:unauthorized)
    end

    it 'gets the user_id from the session' do
      user = create(:user)

      request.headers['Content-Type'] = 'application/vnd.api+json'
      sign_in user
      post :create, format: :json, params: {
        data: {
          type: 'dresses',
          attributes: attributes_for(:dress)
        }
      }

      Rails.logger.error "POST #create: response #{ response.body }"
      expect(response).to have_http_status(:success)
      resp_data = JSON.parse(response.body)

      expect(resp_data['data']['id']).not_to be(nil)
    end
  end

  describe 'PATCH/PUT #update' do
    it 'returns 200 - OK with valid data' do
      dress = create(:dress)

      request.headers['Content-Type'] = 'application/vnd.api+json'
      sign_in dress.user
      patch :update, params: {
        id: dress.id,      # route parameter
        data: {           # actual payload
          id: dress.id,
          type: 'dresses',
          attributes: {
            price: dress.price / 2,
          }
        }
      }

      expect(response).to have_http_status(:ok)
      resp_data = JSON.parse(response.body)

      expect(resp_data['data']['attributes']['price'].to_f).to be_within(0.1).of(dress.price / 2)
    end

    it 'returns 422 - UNPROCESSABLE ENTITY with invalid data' do
      dress = create(:dress)

      request.headers['Content-Type'] = 'application/vnd.api+json'
      sign_in dress.user
      patch :update, params: {
        id: dress.id,      # route parameter
        data: {           # actual payload
          id: dress.id,
          type: 'dresses',
          attributes: {
            email: 'not_jane_doe@email.com',
          }
        }
      }

      expect(response.unprocessable?).to eq(true)
      expect(response).to have_http_status(:unprocessable_entity)
      resp_data = JSON.parse(response.body)

      expect(resp_data['errors'][0]['source']['pointer']).to eq('/data/attributes/exception')
    end
  end

  describe 'DELETE #destroy' do
    it 'returns 204 - NO CONTENT with a valid user id' do
      dress = create(:dress)

      request.headers['Content-Type'] = 'application/vnd.api+json'
      sign_in dress.user
      delete :destroy, params: { id: dress.id }

      expect(response).to have_http_status(:no_content)
      expect { Dress.find(dress.id) }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it 'returns 404 - NOT FOUND with an invalid dress id' do
      dress = create(:dress)

      request.headers['Content-Type'] = 'application/vnd.api+json'
      sign_in dress.user
      delete :destroy, params: { id: 'Z' }

      expect(response).to have_http_status(:not_found)
    end
  end

end
