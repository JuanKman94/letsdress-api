require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  describe 'GET #index' do
    it 'returns a valid list of users with pagination' do
      users = create_list(:user, User.default_per_page * 2)

      sign_in users.first
      get :index

      expect(response).to have_http_status(:ok)
      resp_data = JSON.parse(response.body)

      expect(resp_data['data'].length).to eq(User.default_per_page)
      expect(resp_data['data'][0]['type']).to eq('users')

      # links
      expect(resp_data['links']['prev']).to be(nil)
      expect(resp_data['links']['self']).to eq(resp_data['links']['first'])

      # pagination meta
      expect(resp_data['meta']['prev-page']).to be(nil)
      expect(resp_data['meta']['current-page']).to eq(1)
      expect(resp_data['meta']['next-page']).to eq(2)
      expect(resp_data['meta']['total-pages']).to eq(2)
      expect(resp_data['meta']['total-count']).to eq(User.count)
    end
  end

  describe 'GET #show' do
    it 'returns a valid user' do
      my_user = create(:user)

      sign_in my_user
      get :show, params: { id: my_user.id }, format: :json

      expect(response).to have_http_status(:ok)
      expect(response.content_type).to eq('application/vnd.api+json')

      resp_data = JSON.parse(response.body)

      expect(resp_data['data']['id']).to eq(my_user.id.to_s)
      expect(resp_data['data']['type']).to eq('users')
      expect(resp_data['data']['attributes']['email']).to eq(my_user.email)

      expect(resp_data['data']['links']['self']).to eq(user_url(my_user, {
        host: 'localhost',
        port: 3000
      }))
    end

    it 'returns 404 - NOT FOUND with an invalid user id' do
      user = create(:user)

      sign_in user
      get :show, params: { id: 'Z' }, format: :json

      expect(response).to have_http_status(:not_found)
    end
  end

  describe 'POST #create' do
    it 'returns 406 - NOT ACCEPTABLE with incorrect Content-Type' do
      post :create, params: {}

      expect(response).to have_http_status(:not_acceptable)
    end

    it 'fails with 401 - UNAUTHORIZED without signing in' do
      request.headers['Content-Type'] = 'application/vnd.api+json'
      post :create, format: :json, params: {
        data: {
          type: 'users'
        }
      }

      expect(response).to have_http_status(:unauthorized)
    end

    it 'returns 409 - CONFLICT with invalid data["type"]' do
      user = create(:user)

      request.headers['Content-Type'] = 'application/vnd.api+json'
      sign_in user
      post :create, params: { data: { type: 'not-users' } }

      expect(response).to have_http_status(:conflict)
    end

    it 'returns 422 - UNPROCESSABLE ENTITY with invalid data' do
      user = create(:user)

      request.headers['Content-Type'] = 'application/vnd.api+json'
      sign_in user
      post :create, params: {
        data: {
          type: 'users',
          attributes: {
            email: nil,
            password: nil,
            password_confirmation: nil
          }
        }
      }

      Rails.logger.debug "response.unprocessable? #{ response.unprocessable? }"
      Rails.logger.debug "response.redirection? #{ response.redirection? }"

      expect(response).to have_http_status(:unprocessable_entity)
      resp_data = JSON.parse(response.body)

      pointers = resp_data['errors'].collect { |e|
        e['source']['pointer'].split('/').last
      }.sort

      expect(pointers).to eq([ 'email', 'password' ])
    end

    it 'returns 201 - CREATED with valid data' do
      user = create(:user)

      request.headers['Content-Type'] = 'application/vnd.api+json'
      sign_in user
      post :create, params: {
        data: {
          type: 'users',
          attributes: {
            email: 'Jane_Doe@email.com',
            password: 'super secret',
            password_confirmation: 'super secret'
          }
        }
      }

      expect(response).to have_http_status(:created)
      resp_data = JSON.parse(response.body)

      expect(resp_data['data']['attributes']['email']).to eq('jane_doe@email.com')
    end

  end

  describe 'PATCH/PUT #update' do
    it 'returns 200 - OK with valid data' do
      user = create(:user)

      request.headers['Content-Type'] = 'application/vnd.api+json'
      sign_in user
      patch :update, params: {
        id: user.id,      # route parameter
        data: {           # actual payload
          id: user.id,
          type: 'users',
          attributes: {
            email: 'someone_else@email.com',
          }
        }
      }

      expect(response).to have_http_status(:ok)
      resp_data = JSON.parse(response.body)

      expect(resp_data['data']['attributes']['email']).to eq('someone_else@email.com')
    end

    it 'returns 422 - UNPROCESSABLE ENTITY with invalid data' do
      user = create(:user)

      request.headers['Content-Type'] = 'application/vnd.api+json'
      sign_in user
      patch :update, params: {
        id: user.id,      # route parameter
        data: {           # actual payload
          id: user.id,
          type: 'users',
          attributes: {
            first_name: 'John',
          }
        }
      }

      expect(response).to have_http_status(:unprocessable_entity)
      resp_data = JSON.parse(response.body)

      expect(resp_data['errors'][0]['source']['pointer']).to eq('/data/attributes/exception')
    end
  end

  describe 'DELETE #destroy' do
    it 'returns 204 - NO CONTENT with a valid user id' do
      user = create(:user)

      request.headers['Content-Type'] = 'application/vnd.api+json'
      sign_in user
      delete :destroy, params: { id: user.id }

      expect(response).to have_http_status(:no_content)
      expect { User.find(user.id) }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it 'returns 404 - NOT FOUND with an invalid user id' do
      user = create(:user)

      request.headers['Content-Type'] = 'application/vnd.api+json'
      sign_in user
      delete :destroy, params: { id: 'Z' }

      expect(response).to have_http_status(:not_found)
    end
  end

end
