# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
if Rails.env == "test"
  (1..5).each do |i|
    u = User.find_or_create_by(email: "email_#{ i }@email.com") do |user|
      user.password = 'password'
      user.password_confirmation = 'password'
      user.save!
    end

    u.dresses.find_or_create_by(text: "Really nice dress #{ i }") do |dress|
      dress.price = ( (100.0 - 10.0) * rand() + 10.0 )
    end
  end
end
