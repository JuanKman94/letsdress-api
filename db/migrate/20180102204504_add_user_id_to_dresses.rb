class AddUserIdToDresses < ActiveRecord::Migration[5.1]
  def change
    add_reference :dresses, :user, foreign_key: true
  end
end
