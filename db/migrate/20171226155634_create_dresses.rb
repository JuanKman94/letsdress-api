class CreateDresses < ActiveRecord::Migration[5.1]
  def change
    create_table :dresses do |t|
      t.string :text
      t.decimal :price, precision: 6, scale: 2

      t.timestamps
    end
  end
end
